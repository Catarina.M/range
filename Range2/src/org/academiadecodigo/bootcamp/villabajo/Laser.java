package org.academiadecodigo.bootcamp.villabajo;

import java.util.Iterator;
import java.util.NoSuchElementException;
import java.util.function.Consumer;

public class Laser implements Iterator {

    private int laserMin;
    private int laserMax;
    public Laser(int laserMin, int laserMax) {
        this.laserMax = laserMax;
        this.laserMin = laserMin;
    }

    @Override
    public boolean hasNext() {
        return laserMin <= laserMax;
    }

    @Override
    public Integer next() {
        while(!hasNext()) {
            return null;
        }
        return laserMin++;
    }

    @Override
    public void remove() {
        Iterator.super.remove();
    }

    @Override
    public void forEachRemaining(Consumer action) {
        Iterator.super.forEachRemaining(action);
    }
}
