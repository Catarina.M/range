package org.academiadecodigo.bootcamp.villabajo;

import java.util.Iterator;
import java.util.Spliterator;
import java.util.function.Consumer;

public class Range implements Iterable {
    private int length;
    private Laser laser;
    public Integer max;

    private Integer min;

    private Integer head;

    public Range(Integer min, Integer max) {
        this.min = min;
        this.max = max;

    }

    public Integer getMax() {
        return max;
    }
    public Integer getMin() {
        return min;
    }

    @Override
    public Iterator iterator() {
        return new Laser(this.min,this.max);
    }

    @Override
    public void forEach(Consumer action) {
        Iterable.super.forEach(action);
    }

    @Override
    public Spliterator spliterator() {
        return Iterable.super.spliterator();
    }
}
